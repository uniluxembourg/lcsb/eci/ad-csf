# Chemical curation

## Preamble

This document describes the chemical curation performed to prepare three suspect lists and one database file for use in the suspect and non-target screening of Alzheimer's disease (AD) Cerebrospinal Fluid (CSF) samples, as described in Talavera Andújar et al. (in prep.).

This collaborative effort serves to provide feedback on the development of future workflows in PubChem.

## Set up

Load libraries needed:

    library(RChemMass)
    library(webchem)
    library(data.table)

Then continue on each dataset ...

## Preparation of "AD-CTD"

A list containing 185 CIDs related with AD in the Comparative Toxicogenomics Database (CTD) in PubChem was provided by Tiejun Cheng. Then, unique CIDs were filtered and mapped to the parent CIDs via [PubChem ID Exchange](https://pubchem.ncbi.nlm.nih.gov/idexchange/idexchange.cgi).

The resulting data was stored as a text file ("AD_CTD_PARENT_CID.txt").

Finally, chemical porperties (exact mass, molecular formula, XlogP, InChI, IsomericSMILES, InChIKey, and tittle and IUPAC Name) were obtained via webchem.

    cid_to_parent <- read.delim("AD_CTD_PARENT_CID.txt",header=F)

    unique_parent_cids <- na.exclude(unique(cid_to_parent$V2))
    unique_parent_cids <- as.data.frame(unique_parent_cids)


    # now get the information you need: 
    # Parent_CID    Name    ExactMass   Molecular_Formula   XlogP   InChI   SMILES  InChIKey    IUPAC_Name
    # To check more properties visit: https://pubchemdocs.ncbi.nlm.nih.gov/pug-rest#_Toc494865567

    selected_properties <- c("Title","ExactMass","MolecularFormula","XlogP",
                             "InChI","IsomericSMILES","InChIKey","IUPACName")

    # retrieve info with webchem
    CID_info_all <- as.data.table(webchem::pc_prop(unique_parent_cids, selected_properties))

    # output
    write.csv(CID_info_all,"AD-CTD.csv",row.names = F)

The resulting AD-CTD.csv file contains the 86 CIDs used as suspect list in the article.
Note that Positive and negative charges on the molecular formulas, which cause an error in MetFrag were removed. See Schymanski et al. [work](https://jcheminf.biomedcentral.com/articles/10.1186/s13321-021-00489-0). Moreover, headers were renamed as required for recognition in MetFrag, see [MetFrag website](https://ipb-halle.github.io/MetFrag/projects/metfragcl/) for detailed information about databases format.

## Preparation of "AD-database"

The "AD-database" file was prepared from twenty seven different files provided by Leonid Zaslavsky.These files were .tsv.gz formatted and contained the following columns:

  - Column 1:  MeSH ID value for the query
  - Column 2:  CID value for the neighbor
  - Column 3:  Number of records where both the query and the neighbor are annotated as text entities
  - Column 4:  Cooccurrence score between the query and neighbor entities in the database
  - Column 5:  Name of the neighbor


The files were decompressed and mapped to the parent CIDs via [PubChem ID Exchange](https://pubchem.ncbi.nlm.nih.gov/idexchange/idexchange.cgi).
Then chemical properties were obtained via webchem (as explained above).

Finally, the CIDs from these datasets were merged to form a unique list of CIDs from all files (20968 duplicates, 41917 unique CIDs remained). 
The unique CIDs were used as database (AD-database) for non-target screening in patRoon.  Positive and negative charges were removed and column headers were renamed to facilitate the comprenhension in MetFrag.

The disease MeSH codes,  number of unique entries and description are given in Table 1.


**Table 1:** MeSH-disease-CID datasets forming part of AD-database

| MeSH | CIDs | Disorder |
| --- | --- | --- |
| D000647 | 3,527 | Amnesia |
| D000544 | 14,688 | Alzheimer Disease |
| D000686 | 2,306 | Amyloidosis |
| D001008 | 8,864 | Anxiety Disorders |
| D001037 | 1,074 | Aphasia |
| D001927 | 6,214 | Brain Diseases |
| D002493 | 3,894 | Central Nervous System Diseases |
| D003221 | 5,205 | Confusion |
| D003704 | 6,380 | Dementia |
| D003863 | 378 | Depression |
| D006212 | 2,183 | Hallucinations |
| D006816 | 3,270 | Huntington Disease |
| D007249 | 30,920 | Inflammation |
| D007562 | 746 | Creutzfeldt-Jakob Syndrome |
| D008569 | 6,072 | Memory Disorders |
| D010259 | 1,038 | Paranoid Disorders |
| D013494 | 1,101 | Supranuclear Palsy, Progressive |
| D015140 | 1,513 | Dementia, Vascular |
| D018888 | 112 | Aphasia, Primary Progressive |
| D019636 | 14,896 | Neurodegenerative Diseases |
| D019965 | 2,254 | Neurocognitive Disorders |
| D020232 | 50 | Kluver-Bucy Syndrome |
| D020961 | 1,075 | Lewy Body Disease |
| D055956 | 7 | Diffuse Neurofibrillary Tangles with Calcification |
| D057174 | 434 | Frontotemporal Lobar Degeneration |
| D060825 | 7,336 | Cognitive Dysfunction |
| D064806 | 2,974 | Dysbiosis |

## Filtering the D000544 (AD MeSH term) to obtain the "TOP1" and "SC20" suspect lists
- TOP1 list: this was created with the compounds that have D00054 as its first neighbor. A .tab.gz list containing 1,322 compounds was provided. This was decompressed and mapped to the parent CIDs (via PubChem ID Exchange). Then, duplicates were removed (54) and unique CIDs (1,268) were used to create the TOP1 suspect list. Chemical information was obtained via webchem, positive and negative charges were removed and column headers were renamed as required for recognition in MetFrag.
- SC20 list: this list was created after truncating by 1/20 of the maximum cooccurrence score. A list (.tab.gz  formatted) was provided (containing 321 CIDs). This was decompressed and mapped to the parent CIDs (via PubChem ID Exchange). Then, duplicates were removed (74) and unique CIDs (247) were used to create teh SC20 suspect list. As with TOP1 list, chemical information was obtained via webchem, positive and negative charges were removed and column headers were renamed as required for recognition in MetFrag.

