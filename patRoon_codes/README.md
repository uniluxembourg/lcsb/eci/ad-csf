## Project dependencies

The "renv.lock" file lists all the packages and versions that were installed and used here.

To sep up the same environment, please install the "renv" package in R and run the following command:


    renv::restore()