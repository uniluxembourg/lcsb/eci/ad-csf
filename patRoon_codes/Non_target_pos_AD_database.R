################################Non-target screening of CSF with patRoon 2.1.0###############################


###Load library
library(patRoon)


###Set all options
#Set the path of the working directory
setwd("../../../../patRoon_codes/")

#set the name of the output folder
output <- "report_NT"

# Modify this value to point to the location of the MetFrag2.4.5-CL.jar file
options(patRoon.path.MetFragCL = "~/opt/metfrag/MetFrag2.4.5-CL.jar")

options(patRoon.path.obabel = "~/babel/bin/")


####Initialitation

#Load the analysis.csv that contains the path where the mzML files are. There is one different anaylsis.csv per LC mode (RP or HILIC) and per ionization mode (positive or negative), althought this code is just for the positive mode.

#Positive ionization files
anaInfoPos <- read.csv("analysis_RP_POS.csv")

#Set the localization of the database
myLocalDatabasePath <- ".../patRoon_files/databases/AD_database.csv"


###Extract features
library(xcms)

#To find positive features

fList <- findFeatures(anaInfoPos, "xcms3", param = xcms::CentWaveParam(ppm = 5, peakwidth = c(10, 30), mzdiff = -0.001, mzCenterFun = "wMean", integrate = 1L, snthresh = 10, prefilter = 
                                                                            c(3, 100), noise = 7500, fitgauss = FALSE, firstBaselineCheck = TRUE),verbose = TRUE)

###Group features
library(BiocParallel)
BiocParallel::register(BiocParallel::SerialParam(), default = TRUE)

fGroups <- groupFeatures(fList, "xcms3")

library ("MetaClean") 
fGroups <- filter(fGroups, absMinIntensity = 100,
                  relMinReplicateAbundance = NULL, maxReplicateIntRSD = NULL,
                  blankThreshold = 3, removeBlanks = TRUE,
                  retentionRange = NULL, mzRange = NULL)

###Retrieve MS peak lists
avgPListParams <- getDefAvgPListParams(clusterMzWindow = 0.005)
mslists <- generateMSPeakLists(fGroups, "mzr", maxMSRtWindow = 5, 
                               precursorMzWindow = 0.5, avgFeatParams = 
                               avgPListParams, avgFGroupParams =
                               avgPListParams)

mslists <- filter(mslists, withMSMS = TRUE, absMSIntThr = NULL, absMSMSIntThr = NULL, relMSIntThr = NULL,
                        relMSMSIntThr = NULL, topMSPeaks = NULL, topMSMSPeaks = 25,
							deIsotopeMS = FALSE, deIsotopeMSMS = FALSE)
	
							 
###Find compound structure candidates 							 
compounds <- generateCompounds(fGroups, mslists, "metfrag", method = "CL",
                               dbRelMzDev = 5, fragRelMzDev = 5, 
                               fragAbsMzDev = 0.002, adduct = "[M+H]+", 
                               database = "csv",
                               extraOpts = 
                                 list(LocalDatabasePath = myLocalDatabasePath),
                               scoreTypes = c("fragScore","score", "individualMoNAScore"),
                               maxCandidatesToStop = 500, errorRetries = 200, timeoutRetries = 20) 
							   

###Report results
#Create the csv tables with groups and peak intensities for the set (positive and negative results combined)
reportCSV(fGroups, path = output, reportFeatures = FALSE,
          compounds = compounds, compoundsNormalizeScores = "max")
		  
# Summary of MetFrag Results in a a Single Table 
MFsummary <- as.data.table(compounds)
outputSummary <- paste(output, "MFsummary.csv", sep = "/")
write.csv(MFsummary, outputSummary)

#Plots of every group of features (in the set)
reportPDF(fGroups, path = output, reportFGroups = TRUE, 
          reportFormulaSpectra = TRUE, compounds = compounds, 
          compoundsNormalizeScores = "max",
          MSPeakLists = mslists)