# Alzheimer's disease CSF cheminformatics pipeline



## patRoon non-target and suspect screening

The patRoon_files folder contains all necessary databases and suspect lists.
The patRoon_codes folder contains the codes used to run patRoon:
 - "Non_target_pos_AD_database.R" and "Non_target_neg_AD_database.R": patRoon settings for the non-target analysis with the AD-database in positive and negative ionization modes, respectively.
 - "Non_target_pos_PCL.R" and "Non_target_neg_PCL.R": patRoon settings for the non-target analysis with [PCL](https://doi.org/10.5281/zenodo.6936117) (version 1.12.0) in positive and negative ionization modes, respectively.
 - "Suspect_pos_neg.R":patRoon settings for positive and negative suspect screening analysis. Note that here both modes (positive and negative) are analyzed at the same time.


## Chemical curation
The AD_chem_curation folder contains documentation on the creation of the suspect lists and database files for screening in patRoon.
Note that all chemical files can be found as well in the [Zenodo repository](https://doi.org/10.5281/zenodo.8014420).


## Chemical classification: endogenous/ exogenous
The HMDB_endogenous_exogenous folder contains the curated HMDB database including two extra columns indicating the HMDB categories and subcategories.
The code employed to create the alluvial plot is shared here.
Note that all the lists used for the curation of the HMDB database were downloaded in the [HMDB website](https://hmdb.ca/metabolites).