## patRoon files for non-target and suspect screening

 - databases folder contains the two necessary databases (csv format). "AD-database.csv" is the one employed in the non-target screening while "PubChemLite_exposomics_20220729.csv" is the database used for the suspect screening approaches.
 - suspects folder contains all the suspect lists (csv format)
 - LC_MS_files folder is empty due to ethical issues (human data can not be shared). This folder should contain all the mzML files as well as the analysis.csv, for each different chromatographic method (RP or HILIC) and ionization mode (positive or negative). Example for RP files in positive mode: "LC_MS_files/RP/pos/mzml".
The analysis csv should contain three main columns indicating the location of the mzML files, name, and group (AD, MCI Ctrl or QC).
 

Note that all chemical files can be found as well in the [Zenodo repository](https://doi.org/10.5281/zenodo.8014420).